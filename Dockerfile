FROM alpine:3.21.3

WORKDIR /
COPY requirements.txt .

COPY --from=registry.gitlab.com/sylva-projects/sylva-elements/container-images/sylva-toolbox:v0.8.0 /usr/local/bin/kubectl /usr/local/bin/kubectl

# hadolint ignore=DL3013, DL3018
RUN apk add --no-cache gcc py3-pip py3-netifaces python3 python3-dev musl-dev linux-headers jq curl bash \
    && pip3 install --no-cache-dir --upgrade pip --break-system-packages \
    && pip3 install --no-cache-dir -r requirements.txt --break-system-packages \
    && apk del gcc \
    && apk add yq --no-cache --repository=http://dl-cdn.alpinelinux.org/alpine/edge/community \
    && rm -rf /var/cache/apk/*
